  var originalLineController = Chart.controllers.doughnut;

  Chart.controllers.doughnut = Chart.controllers.doughnut.extend({
    draw: function() {

      originalLineController.prototype.draw.call(this, arguments[0]);

      var ctx = this.chart.chart.ctx;

      var width = this.chart.chart.width,
      height = this.chart.chart.height;

      var fontSize = (height / 200).toFixed(2);
      ctx.font = fontSize + "em Verdana";
      ctx.textBaseline = "middle";
      ctx.fillStyle = "#343C4C"

      var numberText = "30",
      numberTextX = Math.round((width - ctx.measureText(numberText).width) / 2),
      numberTextY = height / 2 - (width / 35);

      ctx.fillText(numberText, numberTextX, numberTextY);


      var fontSize = (height / 250).toFixed(2);
      ctx.font = fontSize + "em Verdana";
      ctx.textBaseline = "middle";
      ctx.fillStyle = "#343C4C"

      var balanceText = "Balance",
      balanceTextX = Math.round((width - ctx.measureText(balanceText).width) / 2),
      balanceTextY = numberTextY + Math.round( ctx.measureText(numberText).width / 2) + (width / 70);

      ctx.fillText(balanceText, balanceTextX, balanceTextY);

    if (this.chart.active !== undefined) {
      if ( this.chart.active.length > 0 ){
          var active = this.chart.active;
          var labels = this.chart.data.labels;

          for (i = 0; i < active.length; i++){

            if (active[i]._datasetIndex === 0) {

              var startAngle = active[i]._model.startAngle;
              var endAngle = active[i]._model.endAngle;
              var middleAngle = startAngle + ((endAngle - startAngle)/2);
              var radius = active[i]._model.outerRadius;

              var deflectionAngle = 0;

              if (middleAngle < 0) {
                deflectionAngle = - (width / 80);
              } else {
                deflectionAngle = width / 100;
              }

              var deflection = 0;

              if (i == 0) {
                deflection = radius / 10;
              } else {
                deflection = radius / 4;
              }

              var firstPosX = (radius - deflection + (width  / 100)) * Math.cos(middleAngle) + width / 2 - deflectionAngle;
              var firstPosY = (radius - deflection + (width  / 100)) * Math.sin(middleAngle) + height / 2;

              var secondPosX = (radius - deflection + (width  / 17)) * Math.cos(middleAngle) + width / 2 -  deflectionAngle;
              var secondPosY = (radius - deflection + (width  / 17)) * Math.sin(middleAngle) + height / 2;

               ctx.beginPath();
               ctx.lineWidth = width / 472;


               ctx.strokeStyle  = shadeRGBColor(active[i]._model.backgroundColor , -0.3);
               ctx.moveTo(firstPosX, firstPosY);
               ctx.lineTo(secondPosX, secondPosY);
               ctx.stroke();
               ctx.closePath();

               ctx.beginPath();
               ctx.arc(secondPosX, secondPosY, width / 354 , 0, 2 * Math.PI, false);
               ctx.fillStyle = shadeRGBColor(active[i]._model.backgroundColor , -0.3);
               ctx.fill();
               ctx.closePath();

               var fontSize = (height / 500).toFixed(2);
               ctx.font = fontSize + "em Verdana";
               ctx.textBaseline = "middle";
               ctx.fillStyle = "#343C4C"

               var textPosX = (radius - deflection + (width  / 13)) * Math.cos(middleAngle) + width / 2 -  deflectionAngle;
               var textPosY = (radius - deflection + (width  / 13)) * Math.sin(middleAngle) + height / 2;

               ctx.fillText(labels[active[i]._index], textPosX, textPosY);


             }
          }
      }

    }

      var segmentsCharts = this.chart.config.data.datasets;

      for (i = 0; i < segmentsCharts.length; i++) {
        segments = segmentsCharts[i]._meta[0].data;
        segmentsData = segmentsCharts[i].data;

        if (i == 0) {
          var fontSize = (height / 350).toFixed(2);
          ctx.font = fontSize + "em Verdana";
          ctx.textBaseline = "middle";
          ctx.fillStyle = "#343C4C"
        } else {
          var fontSize = (height / 400).toFixed(2);
          ctx.font = fontSize + "em Verdana";
          ctx.textBaseline = "middle";
          ctx.fillStyle = "#8F949E"
        }

        for (j = 0; j < segmentsData.length; j++) {

          var startAngle = segments[j]._model.startAngle;
          var endAngle = segments[j]._model.endAngle;
          var middleAngle = startAngle + ((endAngle - startAngle)/2);
          var radius = segments[j]._model.outerRadius;

          var deflection = 0;

          if (i == 0) {
            deflection = radius / 10;
          } else {
            deflection = radius / 4;
          }

          var posX = (radius - deflection) * Math.cos(middleAngle) + width / 2 - (width / 80);
          var posY = (radius - deflection) * Math.sin(middleAngle) + height / 2;

          ctx.fillText(segmentsData[j], posX, posY);

        }
      }

    }
  });

  var config = {
    type: 'doughnut',
    data: {
      datasets: [{
        borderWidth : 0 ,
        doughnutThickness : 60,
        padding : 120,
        data: [
          19,
          23,
          10
        ],
        backgroundColor: [
          "#E2F2E6",
          "#F7F3E4",
          "#F4E7ED"
        ],
        label: 'Dataset 1'
      },  {
        borderWidth : 0 ,
        doughnutThickness : 75,
        padding : 120,
        data: [
          7,
          10,
          2,
          4,
          16,
          3,
          4,
          6
        ],
        backgroundColor: [
          "#14B82B",
          "#70D37F",
          "#B7EABF",
          "#F2BC29",
          "#F7D67E",
          "#FAEBC1",
          "#DA4F7D",
          "#E993AF",
        ],
        label: 'Dataset 2'
      }],
      labels: [
        "Casual Leaves",
        "Sick Leaves",
        "Other Leaves"
      ]
    },
    options: {
      responsive: true,
      legend: {
        display: false
      },
      tooltips: {
        enabled : false
      },
      title: {
        display: false,
        text: 'Chart.js Doughnut Chart'
      },
      animation: {
        animateScale: false,
        animateRotate: false
      },
    }
  };

  function shadeRGBColor(color, percent) {
    var f=color.split(","),t=percent<0?0:255,p=percent<0?percent*-1:percent,R=parseInt(f[0].slice(4)),G=parseInt(f[1]),B=parseInt(f[2]);
    return "rgb("+(Math.round((t-R)*p)+R)+","+(Math.round((t-G)*p)+G)+","+(Math.round((t-B)*p)+B)+")";
  }

  window.onload = function() {
    var ctx = document.getElementById("chart-area").getContext("2d");
    window.myDoughnut = new Chart(ctx, config);
  };
